

function allTheYearaFromData(array){
    if(Array.isArray(array)){
        let years=array.map((element)=>{
            return element.car_year;
        });
        return years;
    };
};

module.exports=allTheYearaFromData;