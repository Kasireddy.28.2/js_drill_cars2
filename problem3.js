

function carModelsListedAlphabetically(array){
    if(Array.isArray(array)){
        let car_models=array.map((element)=>{
            return element.car_model;
        });

        return car_models.sort();
    };

    
};

module.exports=carModelsListedAlphabetically;