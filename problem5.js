
function carsBefore2000Year(array,year){

    if(Array.isArray(array)){
        let cars_before_2000=array.reduce((acc,cv)=>{
            if(cv.car_year===year){
                acc.push(cv.car_model);
            }
            return acc;
        },[]);

        return cars_before_2000;
    };
};

module.exports=carsBefore2000Year;