

function BmwAndAudiCars(array){
    if(Array.isArray(array)){
        let BMW_AUDI_cars=array.reduce((acc,cv)=>{

            if(cv.car_make==="BMW" || cv.car_make==="Audi"){
                acc.push(cv.car_model);
            }
            return acc;

        },[]);

        return BMW_AUDI_cars;
    };
};

module.exports=BmwAndAudiCars;